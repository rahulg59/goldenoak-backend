﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoldenOak.WebAPI.Model
{
     public class ContactUs
    {

        public string Name { get; set; }


        public string Email { get; set; }


        public string Phone { get; set; }


        public string PinCode { get; set; }


        public string Message { get; set; }

    }
}
