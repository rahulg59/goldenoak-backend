﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GoldenOak.Common;
using GoldenOak.ProductDetails;
using GoldenOak.ProductDetails.BAL;
using GoldenOak.ProductDetails.Model;
using GoldenOak.WebAPI.Model;
using HousieGame.Mailer;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GoldenOak.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PartnerController : Controller
    {
        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }




        // POST api/<controller>
        [HttpPost]
        [HttpPost]
        public DefaultResult Post([FromBody]ContactUs value)
        {
            DefaultResult objReturn = new DefaultResult();

            try
            {
                if (value != null)
                {
                    var objEmailTemplate = "Dear, $USERNAME$. .<br /> We will contact you at our earliest, Thank you.";

                    GMailer mailer = new GMailer();
                    mailer.ToEmail = value.Email;
                    mailer.Subject = "You have Applied for Partner Program!!";
                    string Bodyobj = objEmailTemplate.Replace("$USERNAME$", value.Name);
                    mailer.Body = Bodyobj;
                    mailer.IsHtml = true;
                    string strMsg = mailer.Send();


                    //for admin

                    var adminEmailTemplate = "From $USER$ , Email = $EMAIL$ <br/> Phone =  $PHONE$ <br/> Pin Code =  $PIN$<br/> <br/> <br/> Message = $MESSAGE$ <br/>";

                    GMailer a_mailer = new GMailer();
                    a_mailer.ToEmail = "info@goldenoak.in";
                    a_mailer.Subject = value.Name + ", Applied for Partner Program.";
                    string a_Bodyobj = adminEmailTemplate.Replace("$USER$", value.Name).Replace("$MESSAGE$", value.Message).Replace("$EMAIL$", value.Email).Replace("$PHONE$", value.Phone).Replace("$PIN$", value.PinCode);
                    a_mailer.Body = a_Bodyobj;
                    a_mailer.IsHtml = true;
                    string a_strMsg = a_mailer.Send();

                }
            } catch(Exception ex)
            {
                //Comment
            }

            return objReturn;
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
