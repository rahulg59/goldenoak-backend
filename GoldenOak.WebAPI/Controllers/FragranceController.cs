﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GoldenOak.Common;
using GoldenOak.ProductDetails;
using GoldenOak.ProductDetails.BAL;
using GoldenOak.ProductDetails.Model;
using GoldenOak.WebAPI.Model;
using HousieGame.Mailer;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GoldenOak.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FragranceController : Controller
    {
        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }


        // POST api/<controller>
        [HttpPost]
        public DefaultResult Post([FromBody]ProductFragrance value)
        {
            DefaultResult objReturn = new DefaultResult();
            using (ProductDetails_BAL objBAL = new ProductDetails_BAL())
            {
                objReturn.Data = objBAL.InsertUpdateRecordFragrance(value).ToString();
            }
                return objReturn;
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            using (ProductDetails_BAL objBAL = new ProductDetails_BAL())
            {
                objBAL.DeleteFragrance(id);
            }
            return true;
        }
    }
}
