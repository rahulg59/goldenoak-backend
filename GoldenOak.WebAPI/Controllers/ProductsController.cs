﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GoldenOak.Common;
using GoldenOak.ProductDetails;
using GoldenOak.ProductDetails.BAL;
using GoldenOak.ProductDetails.Model;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GoldenOak.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : Controller
    {
        // GET: api/<controller>
        [HttpGet]
        public List<Product> Get()
        {
            List<Product> objReturn = null;
            using (ProductDetails_BAL objBAL = new ProductDetails_BAL())
            {
                objReturn = objBAL.GetAllRecord();
            }
                return objReturn;
        }

        // GET api/<controller>/5
        [HttpGet("{PageNo}/{PageSize}")]
        public ProductFragrancePage Get(int PageNo, string PageSize)
        {
            int iPageSize = Convert.ToInt32(PageSize);
            ProductFragrancePage objReturn = new ProductFragrancePage();
            using (ProductDetails_BAL objBAL = new ProductDetails_BAL())
            {
                objReturn = objBAL.GetRecordPage(PageNo, iPageSize);
            }
            return objReturn;
        }

        [HttpGet("id/{Id}/{PageNo}/{PageSize}")]
        public ProductFragrancePage GetProductByIdPage(Guid Id, int PageNo, string PageSize)
        {
            int iPageSize = Convert.ToInt32(PageSize);
            ProductFragrancePage objReturn = new ProductFragrancePage();
            using (ProductDetails_BAL objBAL = new ProductDetails_BAL())
            {
                objReturn = objBAL.GetProductByIdPage(Id, PageNo, iPageSize);
            }
            return objReturn;
        }

        [HttpGet("category/{Id}/{PageNo}/{PageSize}")]
        public ProductFragrancePage GetProductByCategoryPage(Guid Id, int PageNo, string PageSize)
        {
            int iPageSize = Convert.ToInt32(PageSize);
            ProductFragrancePage objReturn = new ProductFragrancePage();
            using (ProductDetails_BAL objBAL = new ProductDetails_BAL())
            {
                objReturn = objBAL.GetProductByCategoryPage(Id, PageNo, iPageSize);
            }
            return objReturn;
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public Product Get(Guid id)
        {
            Product objReturn = null;

            using (ProductDetails_BAL objBAL = new ProductDetails_BAL())
            {
                objReturn = objBAL.GetRecordById(id);
            }

            return objReturn;
        }

        // GET api/<controller>/5
        [HttpGet("fragrance/{id}")]
        public List<ProductFragrance> GetFragrance(Guid id)
        {
            List<ProductFragrance> objReturn = null;

            using (ProductDetails_BAL objBAL = new ProductDetails_BAL())
            {
                objReturn = objBAL.GetFragrancesById(id);
            }

            return objReturn;
        }

        // POST api/<controller>
        [HttpPost]
        public DefaultResult Post([FromBody]ProductFragranceRel objProduct)
        {
            DefaultResult objReturn = new DefaultResult();

            using (ProductDetails_BAL objBAL = new ProductDetails_BAL())
            {
                objReturn.Data = objBAL.InsertUpdateRecord(objProduct).ToString();
            }

            return objReturn;
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            using (ProductDetails_BAL objBAL = new ProductDetails_BAL())
            {
                objBAL.DeleteRecord(id);
            }
            return true;
        }
    }
}
