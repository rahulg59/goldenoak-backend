﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GoldenOak.Common;
using GoldenOak.ProductDetails;
using GoldenOak.ProductDetails.BAL;
using GoldenOak.ProductDetails.Model;
using GoldenOak.WebAPI.Model;
using HousieGame.Mailer;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GoldenOak.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : Controller
    {
        // GET: api/<controller>
        [HttpGet]
        public List<Category> Get()
        {
            List<Category> objReturn = null;
            using (ProductDetails_BAL objBAL = new ProductDetails_BAL())
            {
                objReturn = objBAL.GetAllCategories();
            }
            return objReturn;
        }

        [HttpGet("{id}")]
        public Category Get(Guid id)
        {
            Category objReturn = null;

            using (ProductDetails_BAL objBAL = new ProductDetails_BAL())
            {
                objReturn = objBAL.GetCategoryById(id);
            }

            return objReturn;
        }

        // POST api/<controller>
        [HttpPost]
        public DefaultResult Post([FromBody]Category value)
        {
            DefaultResult objReturn = new DefaultResult();
            using (ProductDetails_BAL objBAL = new ProductDetails_BAL())
            {
                objReturn.Data = objBAL.InsertUpdateCategory(value).ToString();
            }
                return objReturn;
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            using (ProductDetails_BAL objBAL = new ProductDetails_BAL())
            {
                objBAL.DeleteCategory(id);
            }
            return true;
        }
    }
}
