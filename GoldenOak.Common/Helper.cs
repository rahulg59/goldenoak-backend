﻿using log4net;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace GoldenOak.Common
{
    public class Helper
    {
        ILog log = log4net.LogManager.GetLogger(typeof(Helper));
        public void Dispose()
        {
        }
        public String GetUserIP(ConnectionInfo connection)
        {
            return connection.RemoteIpAddress.ToString();
        }
    }
}
