USE [master]
GO
/****** Object:  Database [GoldenOak]    Script Date: 17-07-2019 17:50:15 ******/
CREATE DATABASE [GoldenOak]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'GoldenOak', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\GoldenOak.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'GoldenOak_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\GoldenOak_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [GoldenOak] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [GoldenOak].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [GoldenOak] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [GoldenOak] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [GoldenOak] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [GoldenOak] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [GoldenOak] SET ARITHABORT OFF 
GO
ALTER DATABASE [GoldenOak] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [GoldenOak] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [GoldenOak] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [GoldenOak] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [GoldenOak] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [GoldenOak] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [GoldenOak] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [GoldenOak] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [GoldenOak] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [GoldenOak] SET  DISABLE_BROKER 
GO
ALTER DATABASE [GoldenOak] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [GoldenOak] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [GoldenOak] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [GoldenOak] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [GoldenOak] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [GoldenOak] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [GoldenOak] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [GoldenOak] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [GoldenOak] SET  MULTI_USER 
GO
ALTER DATABASE [GoldenOak] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [GoldenOak] SET DB_CHAINING OFF 
GO
ALTER DATABASE [GoldenOak] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [GoldenOak] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [GoldenOak] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [GoldenOak] SET QUERY_STORE = OFF
GO
USE [GoldenOak]
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 17-07-2019 17:50:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admin](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AdminId] [uniqueidentifier] NOT NULL,
	[Username] [nvarchar](1000) NULL,
	[Password] [nvarchar](1000) NULL,
	[HostIp] [nvarchar](50) NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[CreationDate] [datetime] NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_Admin] PRIMARY KEY CLUSTERED 
(
	[AdminId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Blog]    Script Date: 17-07-2019 17:50:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Blog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BlogId] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](1000) NULL,
	[Description] [ntext] NULL,
	[FileName] [nvarchar](1000) NULL,
	[FileSize] [nvarchar](1000) NULL,
	[FileType] [nvarchar](1000) NULL,
	[HostIp] [nvarchar](50) NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[CreationDate] [datetime] NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_Blog] PRIMARY KEY CLUSTERED 
(
	[BlogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 17-07-2019 17:50:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [uniqueidentifier] NOT NULL,
	[CategoryName] [nvarchar](1000) NULL,
	[HostIp] [nvarchar](50) NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[CreationDate] [datetime] NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductDetails]    Script Date: 17-07-2019 17:50:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [uniqueidentifier] NOT NULL,
	[CategoryId] [uniqueidentifier] NULL,
	[ProductName] [nvarchar](1000) NULL,
	[ItemCode] [nvarchar](1000) NULL,
	[Package] [int] NULL,
	[Specification1] [nvarchar](1000) NULL,
	[Specification2] [nvarchar](1000) NULL,
	[Specification3] [nvarchar](1000) NULL,
	[FullDescription] [nvarchar](1000) NULL,
	[UseCase] [nvarchar](1000) NULL,
	[HostIp] [nvarchar](50) NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[CreationDate] [datetime] NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_ProductDetails] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductFragranceRel]    Script Date: 17-07-2019 17:50:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductFragranceRel](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [uniqueidentifier] NULL,
	[FragranceName] [nvarchar](1000) NULL,
	[FileName] [nvarchar](1000) NULL,
	[FileSize] [nvarchar](1000) NULL,
	[FileType] [nvarchar](1000) NULL,
	[HostIp] [nvarchar](50) NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[CreationDate] [datetime] NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_ProductFragranceRel] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([Id], [CategoryId], [CategoryName], [HostIp], [CreatedBy], [CreationDate], [UpdatedBy], [UpdatedDate], [IsActive], [IsDeleted]) VALUES (1, N'2dbebd6f-b18b-492d-bdbd-1d61f5901bb9', N'Candles', NULL, NULL, NULL, NULL, NULL, 1, 0)
INSERT [dbo].[Category] ([Id], [CategoryId], [CategoryName], [HostIp], [CreatedBy], [CreationDate], [UpdatedBy], [UpdatedDate], [IsActive], [IsDeleted]) VALUES (3, N'0c75b36e-8b8b-4d75-91cf-2035ffc2799b', N'Gift Sets', NULL, NULL, NULL, NULL, NULL, 1, 0)
INSERT [dbo].[Category] ([Id], [CategoryId], [CategoryName], [HostIp], [CreatedBy], [CreationDate], [UpdatedBy], [UpdatedDate], [IsActive], [IsDeleted]) VALUES (2, N'fe227d28-db65-4f4c-b59d-af5b02172f23', N'Fragrances', NULL, NULL, NULL, NULL, NULL, 1, 0)
SET IDENTITY_INSERT [dbo].[Category] OFF
SET IDENTITY_INSERT [dbo].[ProductDetails] ON 

INSERT [dbo].[ProductDetails] ([Id], [ProductId], [CategoryId], [ProductName], [ItemCode], [Package], [Specification1], [Specification2], [Specification3], [FullDescription], [UseCase], [HostIp], [CreatedBy], [CreationDate], [UpdatedBy], [UpdatedDate], [IsActive], [IsDeleted]) VALUES (1, N'58b96ed4-b2dc-40d7-88f1-1a22de6cd5e6', N'2dbebd6f-b18b-492d-bdbd-1d61f5901bb9', N'Shrink Candles', N'ok', 12, N'ABC', N'DEF', N'GHI', N'KLMNOPQRSTUVW', N'XYZ', NULL, NULL, NULL, NULL, NULL, 1, 0)
SET IDENTITY_INSERT [dbo].[ProductDetails] OFF
SET IDENTITY_INSERT [dbo].[ProductFragranceRel] ON 

INSERT [dbo].[ProductFragranceRel] ([Id], [ProductId], [FragranceName], [FileName], [FileSize], [FileType], [HostIp], [CreatedBy], [CreationDate], [UpdatedBy], [UpdatedDate], [IsActive], [IsDeleted]) VALUES (1, N'58b96ed4-b2dc-40d7-88f1-1a22de6cd5e6', N'Apple', N'f5480574-f0ea-4d17-a073-8a811109ab45.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1)
INSERT [dbo].[ProductFragranceRel] ([Id], [ProductId], [FragranceName], [FileName], [FileSize], [FileType], [HostIp], [CreatedBy], [CreationDate], [UpdatedBy], [UpdatedDate], [IsActive], [IsDeleted]) VALUES (2, N'58b96ed4-b2dc-40d7-88f1-1a22de6cd5e6', N'Apple', N'b4608ad3-5f80-4a43-b07b-32ddba97deaf.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0)
INSERT [dbo].[ProductFragranceRel] ([Id], [ProductId], [FragranceName], [FileName], [FileSize], [FileType], [HostIp], [CreatedBy], [CreationDate], [UpdatedBy], [UpdatedDate], [IsActive], [IsDeleted]) VALUES (3, N'58b96ed4-b2dc-40d7-88f1-1a22de6cd5e6', N'Coffee', N'f49102be-7926-4cba-8990-79aafb209087.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1)
INSERT [dbo].[ProductFragranceRel] ([Id], [ProductId], [FragranceName], [FileName], [FileSize], [FileType], [HostIp], [CreatedBy], [CreationDate], [UpdatedBy], [UpdatedDate], [IsActive], [IsDeleted]) VALUES (4, N'58b96ed4-b2dc-40d7-88f1-1a22de6cd5e6', N'Rose', N'fd58a10e-4ef1-4a4a-8689-0c474bf78f50.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0)
INSERT [dbo].[ProductFragranceRel] ([Id], [ProductId], [FragranceName], [FileName], [FileSize], [FileType], [HostIp], [CreatedBy], [CreationDate], [UpdatedBy], [UpdatedDate], [IsActive], [IsDeleted]) VALUES (5, N'58b96ed4-b2dc-40d7-88f1-1a22de6cd5e6', N'Lemon', N'5c93542a-cfd3-4e0d-a1be-1688affe6ccd.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0)
SET IDENTITY_INSERT [dbo].[ProductFragranceRel] OFF
/****** Object:  StoredProcedure [dbo].[udp_Category_del]    Script Date: 17-07-2019 17:50:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROC [dbo].[udp_Category_del]
	@Id int
AS
SET NOCOUNT ON

UPDATE Category
SET [IsDeleted] = 1 WHERE [Id] = @Id

SET NOCOUNT OFF
GO
/****** Object:  StoredProcedure [dbo].[udp_Category_lst]    Script Date: 17-07-2019 17:50:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROC [dbo].[udp_Category_lst]
AS
SET NOCOUNT ON

SELECT [Id]
	  ,[CategoryId]
	  ,[CategoryName]
      ,[HostIp]
      ,[CreatedBy]
      ,[CreationDate]
      ,[UpdatedBy]
      ,[UpdatedDate]
      ,[IsActive]
      ,[IsDeleted]
  FROM [dbo].[Category] WHERE [IsDeleted] = 0

SET NOCOUNT OFF
GO
/****** Object:  StoredProcedure [dbo].[udp_Category_sel]    Script Date: 17-07-2019 17:50:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROC [dbo].[udp_Category_sel]
	@Id uniqueidentifier
AS
SET NOCOUNT ON

SELECT [Id]
	  ,[CategoryId]
	  ,[CategoryName]
      ,[HostIp]
      ,[CreatedBy]
      ,[CreationDate]
      ,[UpdatedBy]
      ,[UpdatedDate]
      ,[IsActive]
      ,[IsDeleted]
  FROM [dbo].[Category]
WHERE [IsDeleted] = 0 AND [CategoryId] = @Id

SET NOCOUNT OFF
GO
/****** Object:  StoredProcedure [dbo].[udp_Category_ups]    Script Date: 17-07-2019 17:50:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROC [dbo].[udp_Category_ups]
	@Id int,
	@CategoryId uniqueidentifier,
	@CategoryName nvarchar(1000),
	@HostIp nvarchar(50),
	@CreatedBy uniqueidentifier,
	@CreationDate datetime,
	@UpdatedBy uniqueidentifier,
	@UpdatedDate datetime,
	@IsActive bit,
	@IsDeleted bit
AS
SET NOCOUNT ON

IF @Id = 0 BEGIN
	INSERT INTO [Category] (
		[CategoryId]
		,[CategoryName]
		,[HostIp]
		,[CreatedBy]
		,[CreationDate]
		,[UpdatedBy]
		,[UpdatedDate]
		,[IsActive]
		,[IsDeleted]

	)
	VALUES (

	@CategoryId,

	@CategoryName,

	@HostIp,

	@CreatedBy,

	@CreationDate,

	@UpdatedBy,

	@UpdatedDate,

	@IsActive,

	@IsDeleted
	 )
	 SELECT @CategoryId AS InsertedID
END
ELSE BEGIN 
	UPDATE [Category] SET 

		[CategoryId] = @CategoryId,

		[CategoryName] = @CategoryName,

		[HostIp] = @HostIp,

		[CreatedBy] = @CreatedBy,

		[CreationDate] = @CreationDate,

		[UpdatedBy] = @UpdatedBy,
		
		[UpdatedDate] = @UpdatedDate,
		
		[IsActive] = @IsActive,

		[IsDeleted] = @IsDeleted

	WHERE [CategoryId] = @CategoryId
SELECT @CategoryId
END


SET NOCOUNT OFF
GO
/****** Object:  StoredProcedure [dbo].[udp_ProductDetails_byCategory_lstpage]    Script Date: 17-07-2019 17:50:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROC [dbo].[udp_ProductDetails_byCategory_lstpage]
(
@Id uniqueidentifier, @pageNum INT, @pageSize INT, @TotalRecords INT NULL OUTPUT )
AS
SET NOCOUNT ON;

WITH PagingCTE AS
	( SELECT [Id]
	  ,[ProductId]
	  ,[CategoryId]
      ,[ProductName]
	  ,[ItemCode]
	  ,[Package]
	  ,[Specification1]
	  ,[Specification2]
	  ,[Specification3]
	  ,[FullDescription]
	  ,[UseCase]
      ,[HostIp]
      ,[CreatedBy]
      ,[CreationDate]
      ,[UpdatedBy]
      ,[UpdatedDate]
      ,[IsActive]
      ,[IsDeleted]
, ROW_NUMBER() OVER (ORDER BY [Id]) AS RowNumber FROM [ProductDetails] WITH(NOLOCK) WHERE [IsDeleted] = 0 AND CategoryId = @Id

) SELECT PagingCTE.* FROM PagingCTE WHERE RowNumber BETWEEN (@pageNum - 1) * @pageSize + 1 AND @pageNum * @pageSize;
Select @TotalRecords = COUNT([Id]) FROM [ProductDetails] WITH(NOLOCK)	WHERE [IsDeleted] = 0 AND ProductId = @Id
SET NOCOUNT OFF
GO
/****** Object:  StoredProcedure [dbo].[udp_ProductDetails_byId_lstpage]    Script Date: 17-07-2019 17:50:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROC [dbo].[udp_ProductDetails_byId_lstpage]
(
@Id uniqueidentifier, @pageNum INT, @pageSize INT, @TotalRecords INT NULL OUTPUT )
AS
SET NOCOUNT ON;

WITH PagingCTE AS
	( SELECT [Id]
	  ,[ProductId]
      ,[ProductName]
	  ,[ItemCode]
	  ,[Package]
	  ,[Specification1]
	  ,[Specification2]
	  ,[Specification3]
	  ,[FullDescription]
	  ,[UseCase]
      ,[HostIp]
      ,[CreatedBy]
      ,[CreationDate]
      ,[UpdatedBy]
      ,[UpdatedDate]
      ,[IsActive]
      ,[IsDeleted]
, ROW_NUMBER() OVER (ORDER BY [Id]) AS RowNumber FROM [ProductDetails] WITH(NOLOCK) WHERE [IsDeleted] = 0 AND ProductId = @Id

) SELECT PagingCTE.* FROM PagingCTE WHERE RowNumber BETWEEN (@pageNum - 1) * @pageSize + 1 AND @pageNum * @pageSize;
Select @TotalRecords = COUNT([Id]) FROM [ProductDetails] WITH(NOLOCK)	WHERE [IsDeleted] = 0 AND ProductId = @Id
SET NOCOUNT OFF
GO
/****** Object:  StoredProcedure [dbo].[udp_ProductDetails_del]    Script Date: 17-07-2019 17:50:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROC [dbo].[udp_ProductDetails_del]
	@Id int
AS
SET NOCOUNT ON

UPDATE ProductDetails
SET [IsDeleted] = 1 WHERE [Id] = @Id

SET NOCOUNT OFF
GO
/****** Object:  StoredProcedure [dbo].[udp_ProductDetails_lst]    Script Date: 17-07-2019 17:50:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[udp_ProductDetails_lst]
AS
SET NOCOUNT ON

SELECT [Id]
	  ,[ProductId]
	  ,[CategoryId]
      ,[ProductName]
	  ,[ItemCode]
	  ,[Specification1]
	  ,[Specification2]
	  ,[Specification3]
	  ,[FullDescription]
	  ,[UseCase]
      ,[HostIp]
      ,[CreatedBy]
      ,[CreationDate]
      ,[UpdatedBy]
      ,[UpdatedDate]
      ,[IsActive]
      ,[IsDeleted]
  FROM [dbo].[ProductDetails] WHERE [IsDeleted] = 0

SET NOCOUNT OFF
GO
/****** Object:  StoredProcedure [dbo].[udp_ProductDetails_lstpage]    Script Date: 17-07-2019 17:50:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[udp_ProductDetails_lstpage]
(
@pageNum INT, @pageSize INT, @TotalRecords INT NULL OUTPUT )
AS
SET NOCOUNT ON;

WITH PagingCTE AS
	( SELECT [Id]
	  ,[ProductId]
	  ,[CategoryId]
      ,[ProductName]
	  ,[ItemCode]
	  ,[Package]
	  ,[Specification1]
	  ,[Specification2]
	  ,[Specification3]
	  ,[FullDescription]
	  ,[UseCase]
      ,[HostIp]
      ,[CreatedBy]
      ,[CreationDate]
      ,[UpdatedBy]
      ,[UpdatedDate]
      ,[IsActive]
      ,[IsDeleted]
, ROW_NUMBER() OVER (ORDER BY [Id]) AS RowNumber FROM [ProductDetails] WITH(NOLOCK) WHERE [IsDeleted] = 0

) SELECT PagingCTE.* FROM PagingCTE WHERE RowNumber BETWEEN (@pageNum - 1) * @pageSize + 1 AND @pageNum * @pageSize;
Select @TotalRecords = COUNT([Id]) FROM [ProductDetails] WITH(NOLOCK)	WHERE [IsDeleted] = 0
SET NOCOUNT OFF
GO
/****** Object:  StoredProcedure [dbo].[udp_ProductDetails_sel]    Script Date: 17-07-2019 17:50:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[udp_ProductDetails_sel]
	@Id uniqueidentifier
AS
SET NOCOUNT ON

SELECT [Id]
	  ,[ProductId]
	  ,[CategoryId]
      ,[ProductName]
	  ,[ItemCode]
	  ,[Package]
	  ,[Specification1]
	  ,[Specification2]
	  ,[Specification3]
	  ,[FullDescription]
	  ,[UseCase]
      ,[HostIp]
      ,[CreatedBy]
      ,[CreationDate]
      ,[UpdatedBy]
      ,[UpdatedDate]
      ,[IsActive]
      ,[IsDeleted]
  FROM [dbo].[ProductDetails]
WHERE [IsDeleted] = 0 AND [ProductId] = @Id

SET NOCOUNT OFF
GO
/****** Object:  StoredProcedure [dbo].[udp_ProductDetails_ups]    Script Date: 17-07-2019 17:50:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[udp_ProductDetails_ups]
	@Id int,
	@ProductId uniqueidentifier,
	@CategoryId uniqueidentifier,
	@ProductName nvarchar(1000),
	@ItemCode nvarchar(1000),
	@Package int,
	@Specification1 nvarchar(1000),
	@Specification2 nvarchar(1000),
	@Specification3 nvarchar(1000),
	@FullDescription nvarchar(1000),
	@UseCase nvarchar(1000),
	@HostIp nvarchar(50),
	@CreatedBy uniqueidentifier,
	@CreationDate datetime,
	@UpdatedBy uniqueidentifier,
	@UpdatedDate datetime,
	@IsActive bit,
	@IsDeleted bit
AS
SET NOCOUNT ON

IF @Id = 0 BEGIN
	INSERT INTO [ProductDetails] (
		[ProductId]
		,[CategoryId]
		,[ProductName]
		,[ItemCode]
		,[Package]
		,[Specification1]
		,[Specification2]
		,[Specification3]
		,[FullDescription]
		,[UseCase]
		,[HostIp]
		,[CreatedBy]
		,[CreationDate]
		,[UpdatedBy]
		,[UpdatedDate]
		,[IsActive]
		,[IsDeleted]

	)
	VALUES (

	@ProductId,

	@CategoryId,

	@ProductName,

	@ItemCode,

	@Package,

	@Specification1,

	@Specification2,

	@Specification3,

	@FullDescription,

	@UseCase,

	@HostIp,

	@CreatedBy,

	@CreationDate,

	@UpdatedBy,

	@UpdatedDate,

	@IsActive,

	@IsDeleted
	 )
	 SELECT @ProductId AS InsertedID
END
ELSE BEGIN 
	UPDATE [ProductDetails] SET 

		[ProductId] = @ProductId,

		[CategoryId] = @CategoryId,

		[ProductName] = @ProductName,

		[ItemCode] = @ItemCode,

		[Package] = @Package,

		[Specification1] = @Specification1,

		[Specification2] = @Specification2,

		[Specification3] = @Specification3,

		[FullDescription] = @FullDescription,

		[UseCase] = @UseCase,

		[HostIp] = @HostIp,

		[CreatedBy] = @CreatedBy,

		[CreationDate] = @CreationDate,

		[UpdatedBy] = @UpdatedBy,
		
		[UpdatedDate] = @UpdatedDate,
		
		[IsActive] = @IsActive,

		[IsDeleted] = @IsDeleted

	WHERE [ProductId] = @ProductId
SELECT @ProductId
END
GO
/****** Object:  StoredProcedure [dbo].[udp_ProductFragranceRel_del]    Script Date: 17-07-2019 17:50:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROC [dbo].[udp_ProductFragranceRel_del]
	@Id int
AS
SET NOCOUNT ON

UPDATE ProductFragranceRel
SET [IsDeleted] = 1 WHERE [Id] = @Id

SET NOCOUNT OFF
GO
/****** Object:  StoredProcedure [dbo].[udp_ProductFragranceRel_lst]    Script Date: 17-07-2019 17:50:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROC [dbo].[udp_ProductFragranceRel_lst]
AS
SET NOCOUNT ON

SELECT [Id]
	  ,[ProductId]
      ,[FragranceName]
	  ,[FileName]
	  ,[FileSize]
	  ,[FileType]
      ,[HostIp]
      ,[CreatedBy]
      ,[CreationDate]
      ,[UpdatedBy]
      ,[UpdatedDate]
      ,[IsActive]
      ,[IsDeleted]
  FROM [dbo].[ProductFragranceRel] WHERE [IsDeleted] = 0

SET NOCOUNT OFF
GO
/****** Object:  StoredProcedure [dbo].[udp_ProductFragranceRel_lstpage]    Script Date: 17-07-2019 17:50:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROC [dbo].[udp_ProductFragranceRel_lstpage]
(
@pageNum INT, @pageSize INT, @TotalRecords INT NULL OUTPUT )
AS
SET NOCOUNT ON;

WITH PagingCTE AS
	( SELECT [Id]
	  ,[ProductId]
      ,[FragranceName]
	  ,[FileName]
	  ,[FileSize]
	  ,[FileType]
      ,[HostIp]
      ,[CreatedBy]
      ,[CreationDate]
      ,[UpdatedBy]
      ,[UpdatedDate]
      ,[IsActive]
      ,[IsDeleted]
, ROW_NUMBER() OVER (ORDER BY [Id]) AS RowNumber FROM [ProductFragranceRel] WITH(NOLOCK) WHERE [IsDeleted] = 0

) SELECT PagingCTE.* FROM PagingCTE WHERE RowNumber BETWEEN (@pageNum * 1) * @pageSize * 1 AND @pageNum * @pageSize;
Select @TotalRecords = COUNT([Id]) FROM [ProductFragranceRel] WITH(NOLOCK)	WHERE [IsDeleted] = 0
SET NOCOUNT OFF
GO
/****** Object:  StoredProcedure [dbo].[udp_ProductFragranceRel_sel]    Script Date: 17-07-2019 17:50:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[udp_ProductFragranceRel_sel]
	@Id uniqueidentifier
AS
SET NOCOUNT ON

SELECT [Id]
	  ,[ProductId]
      ,[FragranceName]
	  ,[FileName]
	  ,[FileSize]
	  ,[FileType]
      ,[HostIp]
      ,[CreatedBy]
      ,[CreationDate]
      ,[UpdatedBy]
      ,[UpdatedDate]
      ,[IsActive]
      ,[IsDeleted]
  FROM [dbo].[ProductFragranceRel]
WHERE [ProductId] = @Id AND [IsDeleted] = 0

SET NOCOUNT OFF
GO
/****** Object:  StoredProcedure [dbo].[udp_ProductFragranceRel_ups]    Script Date: 17-07-2019 17:50:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[udp_ProductFragranceRel_ups]
	@Id int,
	@ProductId uniqueidentifier,
	@FragranceName nvarchar(1000),
	@FileName nvarchar(1000),
	@FileSize nvarchar(1000),
	@FileType nvarchar(1000),
	@HostIp nvarchar(50),
	@CreatedBy uniqueidentifier,
	@CreationDate datetime,
	@UpdatedBy uniqueidentifier,
	@UpdatedDate datetime,
	@IsActive bit,
	@IsDeleted bit
AS
SET NOCOUNT ON

IF @Id = 0 BEGIN
	INSERT INTO [ProductFragranceRel] (
	  [ProductId]
      ,[FragranceName]
	  ,[FileName]
	  ,[FileSize]
	  ,[FileType]
      ,[HostIp]
      ,[CreatedBy]
      ,[CreationDate]
      ,[UpdatedBy]
      ,[UpdatedDate]
      ,[IsActive]
      ,[IsDeleted]

	)
	VALUES (

	@ProductId,

	@FragranceName,

	@FileName,

	@FileSize,

	@FileType,

	@HostIp,

	@CreatedBy,

	@CreationDate,

	@UpdatedBy,

	@UpdatedDate,

	@IsActive,

	@IsDeleted
	 )
	 SELECT @ProductId AS InsertedID
END
ELSE BEGIN 
	UPDATE [ProductFragranceRel] SET 

		[ProductId] = @ProductId,

		[FragranceName] = @FragranceName,

		[FileName] = @FileName,

		[FileSize] = @FileSize,

		[FileType] = @FileType,

		[HostIp] = @HostIp,

		[CreatedBy] = @CreatedBy,

		[CreationDate] = @CreationDate,

		[UpdatedBy] = @UpdatedBy,
		
		[UpdatedDate] = @UpdatedDate,
		
		[IsActive] = @IsActive,

		[IsDeleted] = @IsDeleted

	WHERE [Id] = @Id
SELECT @ProductId
END


SET NOCOUNT OFF
GO
USE [master]
GO
ALTER DATABASE [GoldenOak] SET  READ_WRITE 
GO
