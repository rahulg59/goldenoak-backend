﻿using Dapper;
using GoldenOak.Connection;
using GoldenOak.ProductDetails.Interface;
using GoldenOak.ProductDetails.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace GoldenOak.ProductDetails.DAL
{
    public class ProductDetails_DAL : IProductDetailsRepository
    {
        ILog log = log4net.LogManager.GetLogger(typeof(ProductDetails_DAL));

        public List<Product> GetAllRecord()
        {
            List<Product> objReturn = null;
            try
            {
                using (SqlConnection db = new SqlDBConnect().GetConnection())
                {
                    objReturn = db.Query<Product>("udp_ProductDetails_lst", commandType: System.Data.CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                log.Error("GetAllRecord Error: ", ex);
            }
            return objReturn;
        }

        //public ProductPage GetRecordPage(int iPageNo, int iPageSize)
        //{
        //    ProductPage objReturn = new ProductPage();
        //    try
        //    {
        //        using (SqlConnection db = new SqlDBConnect().GetConnection())
        //        {
        //            DynamicParameters param = new DynamicParameters();
        //            param.Add("@pageNum", iPageNo);
        //            param.Add("@pageSize", iPageSize);
        //            param.Add("@TotalRecords", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);

        //            objReturn.Products = db.Query<Product>("udp_ProductDetails_lstpage", param: param, commandType: System.Data.CommandType.StoredProcedure).ToList();

        //            objReturn.TotalRecords = param.Get<int>("@TotalRecords");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("GetProductPageList Error: ", ex);
        //    }
        //    return objReturn;
        //}

        public ProductFragrancePage GetRecordPage(int iPageNo, int iPageSize)
        {
            List<List<ProductFragrance>> list = new List<List<ProductFragrance>>();
            ProductFragrancePage objReturn = new ProductFragrancePage();
            try
            {
                using (SqlConnection db = new SqlDBConnect().GetConnection())
                {
                    DynamicParameters param = new DynamicParameters();
                    param.Add("@pageNum", iPageNo);
                    param.Add("@pageSize", iPageSize);
                    param.Add("@TotalRecords", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);

                    objReturn.Products = db.Query<Product>("udp_ProductDetails_lstpage", param: param, commandType: System.Data.CommandType.StoredProcedure).ToList();

                    for(int i = 0; i < objReturn.Products.Count; i++)
                    {
                        List<ProductFragrance> sublist = new List<ProductFragrance>();
                        sublist = GetFragrancesById(objReturn.Products[i].ProductId);
                        list.Add(sublist);
                        objReturn.ProductFragrances = list;
                    }

                    objReturn.TotalRecords = param.Get<int>("@TotalRecords");
                }
            }
            catch (Exception ex)
            {
                log.Error("GetProductPageList Error: ", ex);
            }
            return objReturn;
        }

        public ProductFragrancePage GetProductByIdPage(Guid Id, int iPageNo, int iPageSize)
        {
            List<List<ProductFragrance>> list = new List<List<ProductFragrance>>();
            ProductFragrancePage objReturn = new ProductFragrancePage();
            try
            {
                using (SqlConnection db = new SqlDBConnect().GetConnection())
                {
                    DynamicParameters param = new DynamicParameters();
                    param.Add("@Id", Id);
                    param.Add("@pageNum", iPageNo);
                    param.Add("@pageSize", iPageSize);
                    param.Add("@TotalRecords", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);

                    objReturn.Products = db.Query<Product>("udp_ProductDetails_byId_lstpage", param: param, commandType: System.Data.CommandType.StoredProcedure).ToList();

                    for (int i = 0; i < objReturn.Products.Count; i++)
                    {
                        List<ProductFragrance> sublist = new List<ProductFragrance>();
                        sublist = GetFragrancesById(objReturn.Products[i].ProductId);
                        list.Add(sublist);
                        objReturn.ProductFragrances = list;
                    }

                    objReturn.TotalRecords = param.Get<int>("@TotalRecords");
                }
            }
            catch (Exception ex)
            {
                log.Error("GetProductPageList Error: ", ex);
            }
            return objReturn;
        }

        public ProductFragrancePage GetProductByCategoryPage(Guid CategoryId, int iPageNo, int iPageSize)
        {
            List<List<ProductFragrance>> list = new List<List<ProductFragrance>>();
            ProductFragrancePage objReturn = new ProductFragrancePage();
            try
            {
                using (SqlConnection db = new SqlDBConnect().GetConnection())
                {
                    DynamicParameters param = new DynamicParameters();
                    param.Add("@Id", CategoryId);
                    param.Add("@pageNum", iPageNo);
                    param.Add("@pageSize", iPageSize);
                    param.Add("@TotalRecords", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);

                    objReturn.Products = db.Query<Product>("udp_ProductDetails_byCategory_lstpage", param: param, commandType: System.Data.CommandType.StoredProcedure).ToList();

                    for (int i = 0; i < objReturn.Products.Count; i++)
                    {
                        List<ProductFragrance> sublist = new List<ProductFragrance>();
                        sublist = GetFragrancesById(objReturn.Products[i].ProductId);
                        list.Add(sublist);
                        objReturn.ProductFragrances = list;
                    }

                    objReturn.TotalRecords = param.Get<int>("@TotalRecords");
                }
            }
            catch (Exception ex)
            {
                log.Error("GetProductPageList Error: ", ex);
            }
            return objReturn;
        }

        public List<ProductFragrance> GetFragrancesById(Guid iId)
        {
            List<ProductFragrance> objReturn = null;
            try
            {
                using (SqlConnection db = new SqlDBConnect().GetConnection())
                {
                    DynamicParameters param = new DynamicParameters();
                    param.Add("@Id", iId);
                    objReturn = db.Query<ProductFragrance>("udp_ProductFragranceRel_sel", param: param, commandType: System.Data.CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                log.Error("GetFragrancesById Error: ", ex);
            }
            return objReturn;
        }

        public Product GetRecordById(Guid iId)
        {
            Product objReturn = null;
            try
            {
                using (SqlConnection db = new SqlDBConnect().GetConnection())
                {
                    DynamicParameters param = new DynamicParameters();
                    param.Add("@Id", iId);
                    objReturn = db.Query<Product>("udp_ProductDetails_sel", param: param, commandType: System.Data.CommandType.StoredProcedure).SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                log.Error("GetRecordById Error: ", ex);
            }
            return objReturn;
        }

        public Guid InsertUpdateProduct(Product objProduct)
        {
            Guid objReturn = new Guid();
            try
            {
                using (SqlConnection db = new SqlDBConnect().GetConnection())
                {
                    objReturn = db.Query<Guid>("udp_ProductDetails_ups", param: objProduct, commandType: System.Data.CommandType.StoredProcedure).Single();
                }
            }
            catch (Exception ex)
            {
                log.Error("InsertUpdateRecord Error: ", ex);
            }
            return objReturn;
        }

        public Guid InsertUpdateRecordFragrance(ProductFragrance objProductFragrance)
        {
            Guid objReturn = new Guid();
            try
            {
                using (SqlConnection db = new SqlDBConnect().GetConnection())
                {
                    objReturn = db.Query<Guid>("udp_ProductFragranceRel_ups", param: objProductFragrance, commandType: System.Data.CommandType.StoredProcedure).Single();
                }
            }
            catch (Exception ex)
            {
                log.Error("InsertUpdateRecord Error: ", ex);
            }
            return objReturn;
        }

        //No Use
        public Guid InsertUpdateRecord(ProductFragranceRel objProductFragrance)
        {
            Guid objReturn = new Guid();
            try
            {
                using (SqlConnection db = new SqlDBConnect().GetConnection())
                {
                    objReturn = db.Query<Guid>("udp_ProductFragranceRel_ups", param: objProductFragrance, commandType: System.Data.CommandType.StoredProcedure).Single();
                }
            }
            catch (Exception ex)
            {
                log.Error("InsertUpdateRecord Error: ", ex);
            }
            return objReturn;
        }

        public Guid InsertUpdateRecordForFragrance(ProductFragrance objProductFragrance)
        {
            Guid objReturn = new Guid();
            try
            {
                using (SqlConnection db = new SqlDBConnect().GetConnection())
                {
                    objReturn = db.Query<Guid>("udp_ProductFragranceRel_ups", param: objProductFragrance, commandType: System.Data.CommandType.StoredProcedure).Single();
                }
            }
            catch (Exception ex)
            {
                log.Error("InsertUpdateRecord Error: ", ex);
            }
            return objReturn;
        }

        public bool DeleteRecord(int iId)
        {
            bool objReturn = false;
            try
            {
                using (SqlConnection db = new SqlDBConnect().GetConnection())
                {
                    DynamicParameters param = new DynamicParameters();
                    param.Add("@Id", iId);
                    db.Query("udp_ProductDetails_del", param: param, commandType: System.Data.CommandType.StoredProcedure);
                    objReturn = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("DeleteRecord Error: ", ex);
            }
            return objReturn;
        }

        public bool DeleteFragrance(int iId)
        {
            bool objReturn = false;
            try
            {
                using (SqlConnection db = new SqlDBConnect().GetConnection())
                {
                    DynamicParameters param = new DynamicParameters();
                    param.Add("@Id", iId);
                    db.Query("udp_ProductFragranceRel_del", param: param, commandType: System.Data.CommandType.StoredProcedure);
                    objReturn = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("DeleteRecord Error: ", ex);
            }
            return objReturn;
        }

        public bool DeleteCategory(int iId)
        {
            bool objReturn = false;
            try
            {
                using (SqlConnection db = new SqlDBConnect().GetConnection())
                {
                    DynamicParameters param = new DynamicParameters();
                    param.Add("@Id", iId);
                    db.Query("udp_Category_del", param: param, commandType: System.Data.CommandType.StoredProcedure);
                    objReturn = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("DeleteCategory Error: ", ex);
            }
            return objReturn;
        }

        public List<Category> GetAllCategories()
        {
            List<Category> objReturn = null;
            try
            {
                using (SqlConnection db = new SqlDBConnect().GetConnection())
                {
                    objReturn = db.Query<Category>("udp_Category_lst", commandType: System.Data.CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                log.Error("GetAllCategories Error: ", ex);
            }
            return objReturn;
        }

        public Category GetCategoryById(Guid iId)
        {
            Category objReturn = null;
            try
            {
                using (SqlConnection db = new SqlDBConnect().GetConnection())
                {
                    DynamicParameters param = new DynamicParameters();
                    param.Add("@Id", iId);
                    objReturn = db.Query<Category>("udp_Category_sel", param: param, commandType: System.Data.CommandType.StoredProcedure).SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                log.Error("GetCategoryById Error: ", ex);
            }
            return objReturn;
        }

        public Guid InsertUpdateCategory(Category objCategory)
        {
            Guid objReturn = new Guid();
            try
            {
                using (SqlConnection db = new SqlDBConnect().GetConnection())
                {
                    objReturn = db.Query<Guid>("udp_Category_ups", param: objCategory, commandType: System.Data.CommandType.StoredProcedure).Single();
                }
            }
            catch (Exception ex)
            {
                log.Error("InsertUpdateCategory Error: ", ex);
            }
            return objReturn;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~Product_DAL() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }

}
