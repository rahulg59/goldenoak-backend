﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoldenOak.ProductDetails.Model
{
    public class ProductFragrancePage
    {
        public List<Product> Products { get; set; }

        public List<List<ProductFragrance>> ProductFragrances { get; set; }

        public int TotalRecords { get; set; }
    }
}
