﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoldenOak.ProductDetails.Model
{
   public class Product
    {
        public int Id { get; set; }


        public Guid ProductId { get; set; }


        public Guid CategoryId { get; set; }


        public string ProductName { get; set; }


        public string ItemCode { get; set; }


        public int? Package { get; set; }


        public string Specification1 { get; set; }


        public string Specification2 { get; set; }


        public string Specification3 { get; set; }


        public string FullDescription { get; set; }


        public string UseCase { get; set; }


        public string HostIp { get; set; }


        public Guid? CreatedBy { get; set; }


        public DateTime? CreationDate { get; set; }


        public Guid? UpdatedBy { get; set; }


        public DateTime? UpdatedDate { get; set; }


        public bool? IsActive { get; set; }


        public bool? IsDeleted { get; set; }
    }
}
