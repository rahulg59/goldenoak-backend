﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoldenOak.ProductDetails.Model
{
    public class ProductPage
    {
        public List<Product> Products { get; set; }

        public int TotalRecords { get; set; }
    }
}
