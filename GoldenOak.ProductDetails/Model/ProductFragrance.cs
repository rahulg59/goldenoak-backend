﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoldenOak.ProductDetails.Model
{
   public class ProductFragrance
    {
        public int Id { get; set; }


        public Guid ProductId { get; set; }


        public string FragranceName { get; set; }


        public string FileName { get; set; }


        public string FileSize { get; set; }


        public string FileType { get; set; }


        public string HostIp { get; set; }


        public Guid? CreatedBy { get; set; }


        public DateTime? CreationDate { get; set; }


        public Guid? UpdatedBy { get; set; }


        public DateTime? UpdatedDate { get; set; }


        public bool? IsActive { get; set; }


        public bool? IsDeleted { get; set; }
    }
}
