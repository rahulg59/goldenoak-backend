﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoldenOak.ProductDetails.Model
{
    public class ProductFragranceRelPage
    {
        public List<ProductFragranceRel> ProductFragranceRels { get; set; }

        public int TotalRecords { get; set; }
    }
}
