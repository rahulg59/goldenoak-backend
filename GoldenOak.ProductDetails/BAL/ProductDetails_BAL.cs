﻿using GoldenOak.ProductDetails.Interface;
using GoldenOak.ProductDetails.Model;
using GoldenOak.ProductDetails.DAL;
using GoldenOak.Common;
using log4net;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace GoldenOak.ProductDetails.BAL
{
    public class ProductDetails_BAL : IProductDetailsRepository
    {
        ILog log = log4net.LogManager.GetLogger(typeof(ProductDetails_BAL));

        public List<Product> GetAllRecord()
        {
            List<Product> objReturn = null;
            try
            {
                using (ProductDetails_DAL objDAL = new ProductDetails_DAL())
                {
                    objReturn = objDAL.GetAllRecord();
                }
            }
            catch (Exception ex)
            {
                log.Error("GetAllRecord Error: ", ex);
            }
            return objReturn;
        }

        //public ProductPage GetRecordPage(int iPageNo, int iPageSize)
        //{
        //    ProductPage objReturn = new ProductPage();
        //    try
        //    {
        //        using (ProductDetails_DAL objDAL = new ProductDetails_DAL())
        //        {
        //            objReturn = objDAL.GetRecordPage(iPageNo, iPageSize);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("GetRecordPage Error: ", ex);
        //    }
        //    return objReturn;
        //}

        public ProductFragrancePage GetRecordPage(int iPageNo, int iPageSize)
        {
            ProductFragrancePage objReturn = new ProductFragrancePage();
            try
            {
                using (ProductDetails_DAL objDAL = new ProductDetails_DAL())
                {
                    objReturn = objDAL.GetRecordPage(iPageNo, iPageSize);
                }
            }
            catch (Exception ex)
            {
                log.Error("GetRecordPage Error: ", ex);
            }
            return objReturn;
        }

        public ProductFragrancePage GetProductByIdPage(Guid Id, int iPageNo, int iPageSize)
        {
            ProductFragrancePage objReturn = new ProductFragrancePage();
            try
            {
                using (ProductDetails_DAL objDAL = new ProductDetails_DAL())
                {
                    objReturn = objDAL.GetProductByIdPage(Id, iPageNo, iPageSize);
                }
            }
            catch (Exception ex)
            {
                log.Error("GetRecordPage Error: ", ex);
            }
            return objReturn;
        }

        public ProductFragrancePage GetProductByCategoryPage(Guid Id, int iPageNo, int iPageSize)
        {
            ProductFragrancePage objReturn = new ProductFragrancePage();
            try
            {
                using (ProductDetails_DAL objDAL = new ProductDetails_DAL())
                {
                    objReturn = objDAL.GetProductByCategoryPage(Id, iPageNo, iPageSize);
                }
            }
            catch (Exception ex)
            {
                log.Error("GetRecordPage Error: ", ex);
            }
            return objReturn;
        }

        public Product GetRecordById(Guid iId)
        {
            Product objReturn = null;
            try
            {
                using (ProductDetails_DAL objDAL = new ProductDetails_DAL())
                {
                    objReturn = objDAL.GetRecordById(iId);
                }
            }
            catch (Exception ex)
            {
                log.Error("GetRecordById Error: ", ex);
            }
            return objReturn;
        }

        public List<ProductFragrance> GetFragrancesById(Guid iId)
        {
            List<ProductFragrance> objReturn = null;
            try
            {
                using (ProductDetails_DAL objDAL = new ProductDetails_DAL())
                {
                    objReturn = objDAL.GetFragrancesById(iId);
                }
            }
            catch (Exception ex)
            {
                log.Error("GetFragrancesById Error: ", ex);
            }
            return objReturn;
        }

        public Guid InsertUpdateProduct(Product product)
        {
            Guid objReturn = new Guid();
            return objReturn;
        }

        public List<Category> GetAllCategories()
        {
            List<Category> objReturn = null;
            try
            {
                using (ProductDetails_DAL objDAL = new ProductDetails_DAL())
                {
                    objReturn = objDAL.GetAllCategories();
                }
            }
            catch (Exception ex)
            {
                log.Error("GetAllCategories Error: ", ex);
            }
            return objReturn;
        }

        public Category GetCategoryById(Guid iId)
        {
            Category objReturn = null;
            try
            {
                using (ProductDetails_DAL objDAL = new ProductDetails_DAL())
                {
                    objReturn = objDAL.GetCategoryById(iId);
                }
            }
            catch (Exception ex)
            {
                log.Error("GetCategoryById Error: ", ex);
            }
            return objReturn;
        }

        public Guid InsertUpdateCategory(Category category)
        {
            Guid objReturn = new Guid();
            try
            {
                if (category.Id == 0)
                {
                    category.CategoryId = Guid.NewGuid();
                    category.IsActive = true;
                    category.IsDeleted = false;
                }
                using (ProductDetails_DAL objDAL = new ProductDetails_DAL())
                {
                    objReturn = objDAL.InsertUpdateCategory(category);
                }
            }
            catch (Exception ex)
            {
                log.Error("InsertUpdateCategory Error: ", ex);
            }
            return objReturn;
        }

        public bool DeleteCategory(int iId)
        {
            bool objReturn = false;
            try
            {
                using (ProductDetails_DAL objDAL = new ProductDetails_DAL())
                {
                    objReturn = objDAL.DeleteCategory(iId);
                }
            }
            catch (Exception ex)
            {
                log.Error("DeleteCategory Error: ", ex);
            }
            return objReturn;
        }

        public Guid InsertUpdateRecord(ProductFragranceRel objProductFragrance)
        {
            Guid objReturn = new Guid();
            try
            {
                Product product = new Product();
                ProductFragrance productFragrance = new ProductFragrance();
                
                product.ProductName = objProductFragrance.ProductName;
                product.ItemCode = objProductFragrance.ItemCode;
                product.Package = objProductFragrance.Package;
                product.CategoryId = objProductFragrance.CategoryId;
                product.Specification1 = objProductFragrance.Specification1;
                product.Specification2 = objProductFragrance.Specification2;
                product.Specification3 = objProductFragrance.Specification3;
                product.FullDescription = objProductFragrance.FullDescription;
                product.UseCase = objProductFragrance.UseCase;
                product.IsActive = true;
                product.IsDeleted = false;
                if(objProductFragrance.Id != 0)
                {
                    product.Id = objProductFragrance.Id;
                    product.ProductId = objProductFragrance.ProductId;
                } else
                {
                    product.ProductId = Guid.NewGuid();
                }
                using (ProductDetails_DAL objDAL = new ProductDetails_DAL())
                {
                    if(objProductFragrance.ProductFragranceImage != null)
                    {
                        for (int i = 1; i <= objProductFragrance.ProductFragranceImage.Length; i++)
                        {
                            productFragrance.ProductId = product.ProductId;
                            productFragrance.FragranceName = objProductFragrance.ProductFragranceName[i - 1];
                            productFragrance.FileName = Guid.NewGuid().ToString() + ".png";
                            productFragrance.IsActive = true;
                            productFragrance.IsDeleted = false;
                            AddImage(objProductFragrance.ProductFragranceImage[i - 1], productFragrance.FileName);
                            objReturn = objDAL.InsertUpdateRecordForFragrance(productFragrance);
                        }
                    }
                    objReturn = objDAL.InsertUpdateProduct(product);
                }
            }
            catch (Exception ex)
            {
                log.Error("InsertUpdateRecord Error: ", ex);
            }
            return objReturn;
        }

        public Guid InsertUpdateRecordFragrance(ProductFragrance objProductFragrance)
        {
            Guid objReturn = new Guid();
            try
            {
                if(objProductFragrance.Id == 0)
                {
                    objProductFragrance.FileName = objProductFragrance.FileName.ToString().Split(",")[1];
                    Guid fileName = Guid.NewGuid();
                    AddImage(objProductFragrance.FileName, fileName.ToString() + ".png");
                    objProductFragrance.FileName = fileName.ToString() + ".png";
                }
                using (ProductDetails_DAL objDAL = new ProductDetails_DAL())
                {
                    objReturn = objDAL.InsertUpdateRecordFragrance(objProductFragrance);
                }
            }
            catch (Exception ex)
            {
                log.Error("InsertUpdateRecord Error: ", ex);
            }
            return objReturn;
        }

        public void AddImage(String Image, String FileName)
        {
            String filedir = Path.Combine("wwwroot/images/");

            if (!Directory.Exists(filedir))
            {
                Directory.CreateDirectory(filedir);
            }

            string file = Path.Combine(filedir, FileName);
            if (Image.Length > 100)
            {
                var bytes = Convert.FromBase64String(Image);
                if (bytes.Length > 0)
                {
                    using (var stream = new FileStream(file, FileMode.Create))
                    {
                        stream.Write(bytes, 0, bytes.Length);
                        stream.Flush();
                    }
                }

            }

        }

        public bool DeleteRecord(int iId)
        {
            bool objReturn = false;
            try
            {
                using (ProductDetails_DAL objDAL = new ProductDetails_DAL())
                {
                    objReturn = objDAL.DeleteRecord(iId);
                }
            }
            catch (Exception ex)
            {
                log.Error("DeleteRecord Error: ", ex);
            }
            return objReturn;
        }

        public bool DeleteFragrance(int iId)
        {
            bool objReturn = false;
            try
            {
                using (ProductDetails_DAL objDAL = new ProductDetails_DAL())
                {
                    objReturn = objDAL.DeleteFragrance(iId);
                }
            }
            catch (Exception ex)
            {
                log.Error("DeleteRecord Error: ", ex);
            }
            return objReturn;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~Client_BAL() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
