﻿using GoldenOak.ProductDetails.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace GoldenOak.ProductDetails.Interface
{
    interface IProductDetailsRepository : IDisposable
    {
        List<Product> GetAllRecord();

        ProductFragrancePage GetRecordPage(int iPageNo, int iPageSize);

        Product GetRecordById(Guid iId);

        Guid InsertUpdateRecord(ProductFragranceRel objProduct);

        Guid InsertUpdateProduct(Product objProduct);

        bool DeleteRecord(int iId);
    }
}
